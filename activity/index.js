// NOTE:

// By default all functions are invoked with no parameters
// please use the following guide

// register(user)
// addFriend(user)
// displayFriends()
// getNumberOfFriends()
// deleteLastRegisteredFriend()
// deleteAfriend(user)

/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [
  "James Jeffries",
  "Maggie Williams",
  "Macie West",
  "Michelle Queen",
  "Angelica Smith",
  "Fernando Dela Cruz",
  "Mike Dy",
];

let friendsList = [];
console.log("Registered Users:");
console.log(registeredUsers);
/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
function register(user) {
  if (!user) {
    return;
  }
  const users = registeredUsers.toString();

  if (users.toLowerCase().includes(user.toLowerCase())) {
    alert("Registration failed. Username already exists!");
  } else {
    registeredUsers.push(user);
    alert("Thank you for registering!");
  }
  console.log("Registered Users:");
  console.log(registeredUsers);
}

register();

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/
function addFriend(user) {
  if (!user) {
    return;
  }
  const users = registeredUsers.toString();
  if (users.toLowerCase().includes(user.toLowerCase())) {
    friendsList.push(user);
    alert("You have added " + user + " as a friend!");
  } else {
    alert("User not found");
  }
  console.log("friendsList");
  console.log(friendsList);
}

addFriend();

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
let counter_a = 0;
function displayFriends() {
  counter_a++;

  if (counter_a < 2) {
    return;
  }

  if (!friendsList.length) {
    alert("You currently have 0 friends. Add one first.");
    return;
  }

  friendsList.forEach((friend) => {
    console.log(friend);
  });
}

displayFriends();
// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/
let counter_b = 0;
function getNumberOfFriends() {
  counter_b++;
  if (counter_b < 2) {
    return;
  }

  if (!friendsList.length) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    alert("You currently have " + friendsList.length + " friends.");
  }
}

getNumberOfFriends();
/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/
let counter_c = 0;
function deleteLastRegisteredFriend() {
  counter_c++;
  if (counter_c < 2) {
    return;
  }
  if (!friendsList.length) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    friendsList.pop();
    console.log("friendsList");
    console.log(friendsList);
  }
}

deleteLastRegisteredFriend();
/*======================================================================================*/

// Try this for fun:
/*


    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

function deleteAfriend(user) {
  if (!user) {
    return;
  }
  if (!friendsList.length) {
    alert("You currently have 0 friends. Add one first.");
    return;
  }

  if (friendsList.indexOf(user) !== -1) {
    friendsList.splice(friendsList.indexOf(user), 1);
  } else {
    console.log(
      "Your friend doesn't exist. Please check spelling or capitalization"
    );
  }

  console.log("friendsList");
  console.log(friendsList);
}
deleteAfriend();
